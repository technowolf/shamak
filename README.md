**SHAMAK**

A flask based web app to log IP address of visitor

**USAGE**

After installing all required modules,

1. Run main.py .

2. Use free hosting services like ngrok to host .

3. Share the link for others to visit.

**What will it do.**
1. As soon as you run the main.py , it'll create a database with 2 tables in it.

2. The 1st table will log all the information related to visitor's ISP.

3. The 2nd table will log the nearly exact cordinates of the visitor (Only if he agrees to share the location on website).

4. As soon as it logs all the info it'll redirect the person to a specific link
